import Skype4Py
import time
import importlib
import traceback
import csv
import inspect

skype = Skype4Py.Skype()
skype.Attach()

class Command(object):
    """Command and method object for passive, reactive, and active methods.
    """

    def __init__(self, name=None, method=None, plugin=None, plugin_name=None, docstring=[None]):
        """Set up the Command object.

        Keyword arguments:
        name -- name of command (string)
        method -- method object (method)
        plugin -- plugin object (object)
        plugin_name -- name of plugin (string)
        docstring -- docstring of method as returned by inspect, split by
        newlines (list)
        """

        self.name = name
        self.method = method
        self.plugin = plugin
        self.plugin_name = plugin_name
        self.docstring = docstring
        self.type = self.docstring[0] #active, passive, reactive

class Bot(object):
    """Pluggable Skype bot that interacts with P2P chats."""

    def __init__(self, bot_name):
        """Takes a string."""

        self.name = bot_name
        self.plugins = {} #plugin_name: object
        self.commands = []

        self._load_plugins()

    def _load_plugins(self):
        """Loads all plugins in plugins.csv."""

        with open("plugins.csv", 'r') as f:
            plugins = [line[0] for line in csv.reader(f)]

            for plugin in plugins:
                print "loading", plugin + "..."
                self._load(plugin)

    def _see_chats(self):
        """Returns a list of P2P chats, aka the only kind Skype4Py can use."""
        return [c for c in skype.Chats if c.Topic]

    def _update_plugins(self):
        """Writes a list of keys from self.plugins to plugins.csv."""

        with open("plugins.csv", 'w') as f:
            data = [[p] for p in self.plugins]
            writer = csv.writer(f)

            writer.writerows(data) 

    def _load(self, plugin_name):
        """Loads a plugin and the methods from that plugin by sticking them
        into the commands and plugins dictionaries.

        Takes the string name of a plugin.
        """

        try:
            i = importlib.import_module("plugins.%s" %plugin_name).Class()
        except Exception, e:
            traceback.print_exc()
            print "Failed to load the %s plugin." %plugin_name
            return

        if plugin_name not in self.plugins:
            self.plugins[plugin_name] = i
            plugin = self.plugins[plugin_name]
            plugin.skype = skype
        else:
            return "The specified plugin is already loaded. To reload it, restart me."

        #Get all methods of the plugin.
        methods = [m for m in
                    inspect.getmembers(i, predicate=inspect.ismethod)
                    if "__" not in m[0]]

        for method in methods:

            #There can't be commands with the same name. This actually
            #partially loads the plugin and just stops at the conflicting
            #command. Fix this later. Also, make sure it works at all.
            if method in self.commands:
                return "Plugin not loaded. Conflict with '%s'." %command

            docstring = inspect.getdoc(method[1]).split("\n")
            command_obj = Command(
                                    name=method[0],
                                    method=method[1],
                                    plugin=plugin,
                                    plugin_name=plugin_name,
                                    docstring=docstring
                                    )

            self.commands.append(command_obj)


            print method[0],

        self._update_plugins()

        return "Loaded."

    def _reply(self, chat, message):
        """Takes a chat object and a message object. Reads the message, then
        determines what should be done with it based on plugin commands.
        """

        message.MarkAsSeen()

        body = message.Body
        handle = message.FromHandle

        if "!load" in body:
            msg = self._load(body.split()[1])
            chat.SendMessage(msg)

        for command in self.commands:

            plugin = command.plugin
            plugin.message = message
            plugin.chat = chat

            if body.startswith("!" + command.name) and command.type == 'active':
                command.method()

            if command.type == 'reactive':
                command.method()

    def live(self):
        """Makes the bot read unread messages every second."""

        while True:

            #This is for the passive commands down below, since when we
            #start the bot, there won't be a message to send to plugins
            message = None

            #run _reply on each unread message in P2P chats bot is present in
            for message in [c for c in skype.MissedMessages]:
                try:
                    self._reply(message.Chat, message)

                #handle exceptions so the bot talks about them instead of crashing
                except Exception, e:
                    try:
                        message.Chat.SendMessage("ERROR: " + str(e))
                        traceback.print_exc()
                    except Exception, e:
                        print str(e); traceback.print_exc()

            #run passive commands
            for command in self.commands:
                if command.type == "passive":
                    plugin = command.plugin
                    plugin.message = message
                    plugin.chat = message.chat

                    command.method()

            time.sleep(1)

kokoro = Bot("Kokoro")
kokoro.live()
