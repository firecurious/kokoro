import csv
import os
from textblob import TextBlob
import random

class Message(object):
    """A Skype message from a log."""

    def __init__(self, line):
        """Takes a line from a log csv that looks like this:

        channel name,yyyy-mm-dd hh-mm-ss,username,message body

        Where hh is twenty-four hour time.
        """

        self.chan = line[0]

        date = line[1].replace(" ", "-").split("-")
        self.year = date[0] #yyyy
        self.month = date[1] #mm
        self.day = date[2] #dd
        self.time = date[3] #hh:mm:ss

        self.username = line[2]
        self.body = line[3]

class Corpus(object):
    """A corpus made from the csv log of a channel."""

    def __init__(self, file_name):
        """Takes the name of a chat, like ExtraZero."""

        self.file_name = file_name
        self.messages = []

    def load(self):
        """Loads the corpus from self.file_name."""

        file_name = "logs/" + self._file_name + "_log.csv"

        with open(file_name, 'r') as f:
            self.messages = [line for line in csv.Reader(f)]

class Chatter(object):
    """Plugin to make the bot talk."""

    def __init__(self):
        self.talkative = {} #channel topic: boolean

        self.log_dir = "logs/"
        self.settings_file = "plugins/chatter/settings.csv"
        self.corpora = {} #channel name: Corpus object

        self._build_corpora()
        self._build_settings()

    def _build_corpora(self):
        """Makes Corpus objects of all logs."""

        logs = os.listdir(self.log_dir)

        for log in logs:
            with open(self.log_dir + log, 'r') as f:
                data = [line for line in csv.reader(f)]
                chan_name = data[0][0]

            self.corpora[chan_name] = [Message(line) for line in data
                                        if len(line) <= 500]

    def _build_settings(self):
        """Grabs the settings for self.talkative from settings.csv."""

        with open(self.settings_file) as f:
            data = [line for line in csv.reader(f)]

        for line in data:
            self.talkative[line[0]] = True if line[1] == "True" else False

        print self.talkative

    def _log(self):
        """reactive
        Logs the chat.
        """

        msg = self.message

        # Encoding the topic doesn't actually work
        with open('logs/%s_log.csv' %self.chat.Topic, 'a') as f:
            writer = csv.writer(f)
            writer.writerow([self.chat.Topic,
                                msg.Datetime,
                                msg.FromHandle,
                                msg.Body.encode('utf8')])

    def vocal(self):
        """active
        Sets the value of self.talkative."""

        text = self.message.Body.split("!vocal ")[1]
        msg = None
        topic = self.chat.Topic

        if "true" in text.lower():
            self.talkative[topic] = True
            msg = "I am now talkative."
        else:
            self.talkative[topic] = False
            msg = "Okay. I will stop talking."

        with open(self.settings_file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows([[key, self.talkative[key]] for key in self.talkative])

        self.chat.SendMessage(msg)

    def _night(self):
        """reactive
        Replies with different goodnights when it sees a goodnight message."""

        corpus = self.corpora[self.chat.Topic]
        random.shuffle(corpus)

        if "night" not in self.message.Body.lower():
            return

        for message in corpus:
            text = message.body
            if "night" in text.lower() and len(text.split(" ")) < 3:
                if "fin" not in text.lower():
                    self.chat.SendMessage(text)
                    return

    def _talk(self):
        """reactive
        The bot talks sometimes."""

        topic = self.chat.Topic

        if topic in self.talkative:
            if not self.talkative[topic]:
                return
        else:
            return

        corpus = self.corpora[topic]
        random.shuffle(corpus)

        n = random.randint(1,5)

        if n == 3:
            self.chat.SendMessage(corpus[0].body)

Class = Chatter
