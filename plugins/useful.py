import subprocess
import os
import csv

class Useful(object):
    """Useful commands that don't have their own plugin. Expect that a lot
    of these will only work on Ubuntu. Assuming you have the correct
    programs installed.
    """

    def __init__(self):
        self.commands = {"music": self.music, "volume": self.volume}
        self.reactive = []

        with open("plugins/useful/song.csv", 'r') as f:
            self.song = [l for l in csv.reader(f)][0][0]

    def music(self):
        """active
        Opens Totem on the user's computer and plays music.
        """

        self.totem = subprocess.Popen(["totem", self.song])
        self.chat.SendMessage("Playing music on the user's computer.")

    def volume(self):
        """active
        Say `!volume [up/down]` to change the volume of Totem by 8%.
        """

        body = self.message.Body.split("!volume ")[1]

        if "max" in body:
            for i in range(0, 13):
                os.system("totem --volume-" + "up")

            self.chat.SendMessage("Increased the volume of the music player by 100%.")
            return

        os.system("totem --volume-" + body)
        self.chat.SendMessage("Changed the volume of music player by 8%.")

Class = Useful
