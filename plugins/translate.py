import regex as re
from textblob import TextBlob
import csv

class Words(object):

    def __init__(self):
        """So many words."""

        _file = "/usr/share/dict/words"
        with open(_file, 'r') as f:
            self.english = [word.replace("\n", "") for word in f]

        #code: character
        with open("plugins/translate/morsecode.csv", 'r') as f:
            self.morsecode = {line[1]: line[0] for line in csv.reader(f)}

        #code: language
        with open("plugins/translate/language_codes.csv", 'r') as f:
            self.language_codes = {line[0]: line[1] for line in csv.reader(f)}

        #tag: meaning
        with open("plugins/translate/tags.csv", 'r') as f:
            self.tags = {line[0]: line[1] for line in csv.reader(f)}

class Translate(object):
    """Translates things that are not in English."""

    def __init__(self):
        self.commands = {"language": self.language, "diagram": self.diagram}
        self.reactive = [self._translate]

        
        self.words = Words().english

    def _german(self, text):
        """Translates all languages, but this function is still called German..."""
        blob = TextBlob(text)

        try:
            return str(blob.translate(to="en"))
        except:
            return text

    def _rot13(self, text):
        """Translates rot13."""

        try:
            return text.encode("rot13")
        except: #unicode error; garbage characters
            return text

    def _doublefrench(self, text):
        """Translates Double French."""

        if "os" in text.lower():
            print "os detected"
            text = text.replace("os", "").replace("Os", "").replace("OS", "")

        return text

    def _morsecode(self, text):
        """Translates Morse code.

        Test case: .. - / .. ... / - .. -- . / ..-. --- .-. / -... . -.. .-.-.- / --. --- --- -.. -. .. --. .... - / -. --- .--"""

        #ignore things that are probably not actual Morse code messages
        if text in ["...", "."]:
            return text

        #ignore links!
        if "http://" in text or "https://" in text:
            return text

        code = Words().morsecode

        text = [code[char] if char in code else char for char in text.split(" ")]
        text = " ".join("".join(text).split("/"))

        return text

    def _strip(self, text):
        """Strips the punctuation from a given string and returns as a list."""

        text = re.sub(ur"\p{P}+", "", text) #strip punctuation
        text = text.split()
        return text

    def _garbage(self, text):
        """Checks if some text does not seem to be in English."""

        text = self._strip(text)
        garbage_words = [word for word in text if word.lower() not in self.words]

        if len(garbage_words) >= (len(text) - len(garbage_words)):
            return True
        else:
            return False

    def _better(self, original_text, translation):
        """Compares two strings to see which one has more English words and
        returns True or False.
        """

        original_text = self._strip(original_text)
        translation = self._strip(translation)

        orig_n = len([w for w in original_text if w.lower() in self.words])
        trans_n = len([w for w in translation if w.lower() in self.words])

        if trans_n > orig_n:
            return True
        else:
            return False

    def _translate(self):
        """reactive"""

        translations = [self._doublefrench, self._rot13, self._german, self._morsecode]

        text = self.message.Body

        if self._garbage(text):
            for method in translations:
                translation = method(text)

                if self._better(text, translation):
                    self.chat.SendMessage("Translation: " + translation)

    def language(self):
        """active
        Detects the language of the given text."""

        lan_codes = Words().language_codes

        text = TextBlob(self.message.Body.split("!language ")[1])

        #"Must provide a string with at least 3 characters." but some words
        #are too short
        if len(text) < 3:
            text = text + ".."

        lan_code = text.detect_language()

        language = lan_codes[lan_code] if lan_code in lan_codes else "unknown"
        self.chat.SendMessage(lan_code + ", " + language)

    def diagram(self):
        """active
        Displays all of the TextBlob tags for a piece of text."""

        tags = Words().tags
        text = TextBlob(self.message.Body.split("!diagram ")[1])
        self.chat.SendMessage(" ".join(["%s (%s)"
                                %(word, tags[tag] if tag in tags else tag)
                                for word, tag in text.tags]))

Class = Translate
