class Chatextras(object):
    """Plugin to make the bot talk."""

    def __init__(self):
        self.commands = {
                            "parrot": self.parrot,
                            "analyze": self.analyze
                        }
        self.reactive = [self._boring_chat]

    def _boring_chat(self):
        """reactive"""
        
        if "night" in self.message.Body.lower():
            #self.chat.SendMessage("Goodnight!")

            if "fin" in self.message.Body.lower():
                self.chat.SendMessage("Final goodnight!")

        if "kokoro" in self.message.Body.lower():
            self.chat.SendMessage("Hello.")

    def analyze(self):
        """active
        Analyze a piece of text using TextBlob.
        """

        text = self.message.Body.split("!analyze ")[1]
        text = TextBlob(text)
        self.chat.SendMessage(str(text.tags) + " " + str(text.sentiment))

    def parrot(self):
        """active
        Repeats the provided text.
        """

        text = self.message.Body.split("!parrot ")[1]

        self.chat.SendMessage(text)

Class = Chatextras
